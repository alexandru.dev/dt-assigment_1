//
// Created by alexandru.iliescu on 10/22/2019.
//

#ifndef PARSER_H
#define PARSER_H
#define MAX_COLS 100
#define MAX_LINES 100

typedef struct{
    uint8_t data[MAX_LINES][MAX_COLS-1];
    uint8_t number_of_lines;
    uint8_t number_of_columns;
    uint8_t ok; //1=ok, 0=error
}AT_CMD_DATA;

AT_CMD_DATA date;

typedef enum {
    STM_OK_NOT_READY, STM_READY_OK, STM_READY_ERROR;
}AT_CMD_RESPONSE;

AT_CMD_RESPONSE parse(uint8_t current_char);

#endif //ASSIGMENT_1_PARSER_H
