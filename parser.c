//
// Created by alexandru.iliescu on 10/22/2019.
//

#include "parser.h"
#include <stdio.h>

AT_CMD_RESPONSE parse(uint8_t current_char)
{
    static uint8_t state = 0;
    switch (state)
    {
        case 0: if(current_char == 13) {
                    state = 1;
                }
                break;
        case 1: if(current_char == 10){
            state = 2;
        }else {return STM_READY_ERROR;}
        case 2: if(current_char == 79){
            state = 3;
        }
        else if(current_char == 69){
            state = 7;
        }
        else if(current_char == 43){
            state = 14;
        }
        else {return STM_READY_ERROR;}

        case 3: if(current_char == 75){
            state = 4;
        }
        else {return STM_READY_ERROR;}
        case 4: if(current_char == 13){
            state = 5;
        }
        else {return STM_READY_ERROR;}
        case 5: if(current_char == 10){
            state = 6;
        }
        else {return STM_READY_ERROR;}
        case 6: return STM_READY_OK;
        case 7: if(current_char == 82){
            state = 8;
        }
        else{return STM_READY_ERROR;}
        case 8: if(current_char == 82){
            state = 9;
        }
        else {return STM_READY_ERROR;}
        case 9: if(current_char == 79){
            state = 10;
        }else {return STM_READY_ERROR;}
        case 10: if(current_char == 82){
            state = 11;
        }
        else {return STM_READY_ERROR;}
        case 11: if(current_char == 13){
                state = 12;
            }
        else {return STM_READY_ERROR;}
        case 12: if(current_char == 10){
                state = 13;
            }else {return STM_READY_ERROR;}
        case 13: return STM_READY_OK;
        case 14: if(current_char >= 32 && current_char <= 122){
            if(date.number_of_lines < 100 && date.number_of_columns < 100){
                date.number_of_columns = 0;
                date.number_of_lines++;
                date.data[date.number_of_lines][date.number_of_columns] = current_char;
                date.number_of_columns++;
            }
                state = 15;
        } else {return STM_READY_ERROR;}
        case 15: if(current_char == 13) {
                date.data[date.number_of_lines][date.number_of_columns] = '\0';
                state = 16;
            } else if(current_char >= 32 && current_char <= 122) {
                if(date.number_of_lines < 100 && date.number_of_columns < 100){
                    date.number_of_lines++;
                    date.data[date.number_of_lines][date.number_of_columns] = current_char;
                    date.number_of_columns++;
                }
                break;}
                else {return STM_READY_ERROR;}
        case 16: if(current_char == 10) {
            state = 17;
        } else {return STM_READY_ERROR;}
        case 17: if(current_char == 13) {
            state = 18;
        } else if (current_char == 43) {
            state = 14;
        } else {return STM_READY_ERROR;}
        case 18: if(current_char == 10) {
            state = 19;
        } else { return STM_READY_ERROR;}
        case 19: if(current_char == 69){
            state = 7;
        } else if(current_char == 79){
            state = 3;
        }else{ return STM_READY_ERROR;}
    }
    return STM_OK_NOT_READY;
}